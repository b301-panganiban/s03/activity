import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        int answer = 1;

        System.out.println("Input an integer whose factorial will be computed:");
        Scanner in = new Scanner(System.in);

        // While
//        try {
//            int num = in.nextInt();
//
//            if (num == 0) {
//                System.out.println("The factorial of 0 is 1");
//            } else if (num < 0) {
//                System.out.println("Cannot compute for factorials of negative numbers.");
//            } else {
//                while (counter <= num) {
//                    answer *= counter;
//                    counter++;
//                }
//                System.out.println("The factorial of " + num + " is " + answer);
//            }
//        } catch (Exception e) {
//            System.out.println("Invalid input, this program only accepts integer inputs.");
//            e.printStackTrace();
//        }


        //For loop
        try {
            int num = in.nextInt();
            if(num == 0) {
                System.out.println("The factorial of 0 is 1");
            } else if (num < 0) {
                System.out.println("Cannot compute for factorials of negative numbers.");
            } else {
                for (int counter = 1; counter <= num; counter++) {
                    answer *= counter;
                }
                System.out.println("The factorial of " + num + " is " +answer);
            }
        } catch (Exception e) {
            System.out.println("Invalid input, this program only accepts integer inputs.");
            e.printStackTrace();
        }
    }
}